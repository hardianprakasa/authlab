//
//  FriendsTableViewCell.swift
//  AuthLab
//
//  Created by Hardian Prakasa on 6/9/17.
//  Copyright © 2017 Hardian Prakasa. All rights reserved.
//

import UIKit
import SDWebImage

class FriendsTableViewCell: UITableViewCell {

    @IBOutlet weak fileprivate var photosImageView: UIImageView!
    @IBOutlet weak fileprivate var nameLabel: UILabel!
    
    func configureCell(friend: Friend) {
        photosImageView.sd_setImage(with: URL(string: friend.thumbnailURL), placeholderImage: UIImage(named:"ImagePlaceholder"))
        nameLabel.text = friend.name
    }
    
}
