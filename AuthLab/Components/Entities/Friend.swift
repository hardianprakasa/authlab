//
//  Friend.swift
//  AuthLab
//
//  Created by Hardian Prakasa on 6/9/17.
//  Copyright © 2017 Hardian Prakasa. All rights reserved.
//

import Foundation

struct Friend {
    let id: Int
    let name: String
    let thumbnailURL: String
}
