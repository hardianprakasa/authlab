//
//  LoginViewController.swift
//  AuthLab
//
//  Created by Hardian Prakasa on 6/7/17.
//  Copyright © 2017 Hardian Prakasa. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyJSON

class LoginViewController: UIViewController {

    @IBOutlet weak fileprivate var FBLoginButton: FBSDKLoginButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Login"
        configureLoginButton()
    }
    
    fileprivate func configureLoginButton() {
        FBLoginButton.readPermissions = ["user_friends", "email"]
        FBLoginButton.delegate = self
    }
    
}

extension LoginViewController: FBSDKLoginButtonDelegate {
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        guard (FBSDKAccessToken.current()) != nil else {
            return
        }
        
        navigationController?.setViewControllers([FriendsViewController()], animated: true)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("logout")
    }
}
