//
//  FriendsViewController.swift
//  AuthLab
//
//  Created by Hardian Prakasa on 6/8/17.
//  Copyright © 2017 Hardian Prakasa. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SVProgressHUD
import FBSDKLoginKit
import FBSDKShareKit

class FriendsViewController: UIViewController {

    @IBOutlet weak fileprivate var friendsTableView: UITableView!
    @IBOutlet weak fileprivate var uploadPictureButton: UIButton!
    
    let viewModel = FriendsViewModel()
    let disposeBag = DisposeBag()
    
    let loginManager = FBSDKLoginManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = viewModel.title
        
        friendsTableView.delegate = self
        friendsTableView.dataSource = self
        friendsTableView.register(UINib(nibName: "FriendsTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        configureRighBarButtonItem()
        configureViewModelObserver()
        
        viewModel.getFriends()
    }
    
    fileprivate func configureRighBarButtonItem() {
        let logoutButtonItem = UIBarButtonItem(image: UIImage(named: "LogoutIcon"), style: .plain, target: self, action: #selector(logoutAction))
        logoutButtonItem.tintColor = UIColor.red
        
        let uploadButtonItem = UIBarButtonItem(image: UIImage(named: "UploadIcon"), style: .plain, target: self, action: #selector(uploadAction))
        
        navigationItem.rightBarButtonItems = [logoutButtonItem, uploadButtonItem]
    }
    
    fileprivate func configureViewModelObserver() {
        viewModel.isShowHud
            .asDriver()
            .drive(onNext: { show in
                show ? SVProgressHUD.show() : SVProgressHUD.dismiss()
            })
            .addDisposableTo(disposeBag)
        
        viewModel.friends
            .asDriver()
            .drive(onNext: { [weak self] friends in
                self?.friendsTableView.reloadData()
            })
            .addDisposableTo(disposeBag)
    }
    
    @objc fileprivate func logoutAction() {
        loginManager.logOut()
        navigationController?.setViewControllers([LoginViewController()], animated: true)
    }
    
    @objc fileprivate func uploadAction() {
        let picker = UIImagePickerController()
        
        picker.delegate = self
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.modalPresentationStyle = .fullScreen
        
        present(picker, animated: true)
    }
}

extension FriendsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = friendsTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? FriendsTableViewCell else {
            return UITableViewCell()
        }
        
        let friend = viewModel.friends.value[indexPath.row]
        
        cell.configureCell(friend: friend)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.friends.value.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.rowHeight
    }
}

extension FriendsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        dismiss(animated: true)
        
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            print("Info did not have the required UIImage for the Original Image")
            return
        }
        
        let photo = FBSDKSharePhoto()
        photo.image = image
        photo.isUserGenerated = true
        
        let content = FBSDKSharePhotoContent()
        content.photos = [photo]
        
        let dialog = FBSDKShareDialog()
        dialog.fromViewController = self
        dialog.shareContent = content
        dialog.mode = .automatic
        dialog.show()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
