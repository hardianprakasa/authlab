//
//  FriendsViewModel.swift
//  AuthLab
//
//  Created by Hardian Prakasa on 6/9/17.
//  Copyright © 2017 Hardian Prakasa. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import SwiftyJSON
import FBSDKCoreKit

class FriendsViewModel {
    
    let title = "Friends"
    
    var isShowHud = Variable<Bool>(false)
    var friends = Variable<[Friend]>([])
    
    let rowHeight = CGFloat(95)
    
    func getFriends() {
        isShowHud.value = true
        
        let params = ["fields": "id, name, picture"]
        
        let graphRequest = FBSDKGraphRequest(graphPath: "/me/friends", parameters: params)
        let connection = FBSDKGraphRequestConnection()
        
        connection.add(graphRequest, completionHandler: { [weak self] (connection, result, error) in
            guard error == nil else {
                print("Error Getting Friends \(String(describing: error))")
                return
            }
            
            let json = JSON(result ?? "")
            let userData = json["data"]
            
            self?.friends.value = userData.arrayValue.map({ data -> Friend in
                let friend = Friend(id: data["id"].intValue,
                                    name: data["name"].stringValue,
                                    thumbnailURL: data["picture"]["data"]["url"].stringValue)
                
                return friend
            })
            
            self?.isShowHud.value = false
        })
        
        connection.start()
    }
    
}
